# Se define s.o. que se usará en la imagen, por su peso liviano se elige una variante alpine
FROM node:current-alpine3.12

# Se define el directorio en donde se trabajara y copiara la aplicacion
WORKDIR /usr/src/app

# Se copian todos los archivos con extension .json

COPY package*.json ./

# Se instalan las dependencias
RUN npm install

#Se copia toda la aplicacion al directorio actual /usr/src/app
COPY . .

# Se ejecuta test
RUN npm run test

# Se expone el puerto solicitado por el reto
EXPOSE 3000

# Seleccionar usuario node para ejecutar la aplicacion
USER node

# CMD para mantener funcionando el container
CMD [ "node", "index.js" ]
